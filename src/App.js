import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import HomeContainer from './Home';
import NoMatchContainer from './NoMatch';
import StatisticContainer from './Statistics';

class App extends Component {
  render() {

    return (
      <Router>
        <div className="App">
          <div className="navigation-content">
            <div className="navigation">
              <Link to="/">Home Page</Link>
              <Link to="/statistics">Show Statistics</Link>
            </div>
          </div>
          <Switch>
            <Route path="/" exact component={HomeContainer} />
            <Route path="/" component={StatisticContainer} />
            <Route component={NoMatchContainer} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
