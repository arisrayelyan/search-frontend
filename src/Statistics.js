import React, { Component } from 'react';
import StatisticContent from './components/StatisticContent'
import axios from 'axios';

class Statistics extends Component {
    constructor() {
        super();

        this.state = {
            isLoading: true,
            data: [],
            isError: null
        };
    }

    componentDidMount(){
        axios(process.env.REACT_APP_API_URL + '/statistics')
        .then(result => {
          this.setState({
            data: result.data.data,
            isLoading: false,
            isError: null
          });
        })
        .catch(error => {
            console.log(error.toString())
          this.setState({
            data: [],
            isLoading: false,
            isError: error.toString()
          });
        })
    }

    render() {
        
        return (
            <div className="statistics-page">
                <h2>Statistics</h2>
                <StatisticContent isError={this.state.isError} data={this.state.data} loading={this.state.isLoading} />
            </div>
        );
    }
}

export default Statistics;