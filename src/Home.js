import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Search from './Search'
import queryString from "query-string";



class Home extends Component {

    
    render() {
        const {q} = queryString.parse(this.props.location.search);
        return (
            <div className="main">
                <div className="search-content">
                    <div className="search-field">
                        <Search
                            query={q}
                        />
                    </div>
                    <div />
                </div>
            </div>
        );
    }
}

export default withRouter(Home);