import React, { Component } from 'react';

class NoMatch extends Component {
    render() {
        return (
            <div className="notfound">
                <div className="notfound-code">404</div>
                <p>SORRY BUT THE PAGE YOU ARE LOOKING FOR DOES NOT EXIST, HAVE BEEN REMOVED. NAME CHANGED OR IS TEMPORARILY UNAVAILABLE</p>
            </div>
        );
    }
}

export default NoMatch;