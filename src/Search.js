import Autosuggest from 'react-autosuggest';
import React from 'react';
import axios from 'axios'
import Items from './components/Items'
import './styles/autoSuggestStyle.css'
import { withRouter } from 'react-router-dom';

/**
 * Get Suggestion Value from result
 * @param {Object} suggestion 
 * @returns {String} Suggestion Title
 */
const getSuggestionValue = suggestion => suggestion.position_title;

/**
 * Autocomplite results structure
 * @param {Object} suggestion 
 * @returns {HTMLElement} Suggestion row structure
 */
const renderSuggestion = suggestion => (
  <div>
    {suggestion.position_title}
  </div>
);

class Search extends React.Component {

  constructor() {
    super();

    this.state = {
      value: '',
      suggestions: [],
      isEmpty: false,
      isLoading: false,
      data: [],
      content: [],
      contentLoading: false
    };
  }


  componentDidMount() {

    //check if query param is exists and retrive data from the api
    if (this.props.query) {

      this.setState({
        value: this.props.query,
        isLoading: true
      });

      axios(process.env.REACT_APP_API_URL + '/search?q=' + this.props.query)
        .then(result => {
          this.setState({
            content: result.data.data,
            isLoading: false
          });
        })
        .catch(error => {
          this.setState({
            content: [],
            isLoading: false
          });
        })

    }
  }

  /**
   * On Change handler
   */
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  // Autosuggest will call this function every time you need to update suggestions.
  onSuggestionsFetchRequested = ({ value }) => {

    if (value.length >= 3) {
      //enable loader
      this.setState({
        isLoading: true
      });
      axios(process.env.REACT_APP_API_URL + '/search?q=' + value)
        .then(result => {
          this.setState({
            suggestions: result.data.data.slice(0, 5), //show max 5 result
            isEmpty: result.data.data.length === 0,
            isLoading: false
          });
        })
        .catch(error => {
          this.setState({
            suggestions: [],
            isLoading: false
          });
        })
    }

  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  /**
   * On Suggestion select event function
   */
  onSuggestionSelected = (event, { suggestionValue }) => {
    
    if(suggestionValue !== ''){
      this.setState({contentLoading: true})
      axios(process.env.REACT_APP_API_URL + '/search?q=' + suggestionValue)
        .then(result => {
          this.setState({
            content: result.data.data,
            contentLoading: false
          })
        })
        .catch(error => {
          this.setState({
            content: [],
            contentLoading: false
          })
        })

        //save keywords
        axios.post(process.env.REACT_APP_API_URL + '/save?',{keyword:suggestionValue})
        .then(res => {console.log(res.data)})
        .catch(error => console.log(error.toString()))

    }

    //push selected result to url
    this.props.history.push({
      pathname: '/',
      search: "?" + new URLSearchParams({ q: suggestionValue }).toString()
    })

  }

  render() {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Type a job title',
      value,
      onChange: this.onChange
    };


    return (
      <div>
        <div className="search-header">
          <h2>Search Job {this.state.isLoading && 'Loading...'}</h2>
          <p>Keyword must be of minimum 3 characters length</p>
          <p>Example -> type manager or developer</p>
        </div>
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={inputProps}
          onSuggestionSelected={this.onSuggestionSelected}
        />
        <Items items={this.state.content} loading={this.state.contentLoading} />
      </div>
    );
  }
}

export default withRouter(Search);