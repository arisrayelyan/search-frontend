import React from 'react';
import { shallow } from 'enzyme';
import Items from './';



it('renders without crashing', () => {
    shallow(<Items items={[]} />);
});

it('renders loading progress bar', () => {
    const wrapper = shallow(<Items items={[]} loading={true} />);
    const noResult = <div className="items-content">Loading...</div>;
    expect(wrapper.contains(noResult)).toEqual(true);
});
