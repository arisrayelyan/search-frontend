import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './items.css';

class Items extends Component {
    render() {
        const {items,loading} = this.props;

        if(loading){
            return (<div className="items-content">Loading...</div>);
        }

        if(!loading && items.length === 0){
            return ('');
        }

        return (
            <div className="items-content">
            {items.map((res,index) => 

                <div key={index} className="items-block">
                    <table>
                        <tbody>
                        <tr>
                            <th>Job Title:</th>
                            <td>{res.position_title}</td>
                        </tr>
                        <tr>
                            <th>Organization Name:</th>
                            <td>{res.organization_name}</td>
                        </tr>
                        <tr>
                            <th>Link:</th>
                            <td><a href={res.url}>Go to Job Page</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            )}
                
            </div>
        );
    }
}


Items.propTypes = {
    /**
     * Items.
     */
    items: PropTypes.array.isRequired,
}

export default Items;