import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './statistics.css';

/**
 * Component for Drawing Statistics table 
 */
class StatisticContent extends Component {
    render() {
        const { data, loading, isError } = this.props;

        if (loading) {
            return ('Loading ...');
        }
        if (!loading && !isError && data.length === 0) {
            return ("No Result");
        }
        if (!loading && isError) {
            return (<div>{isError}</div>);
        }

        return (
            <div className="statistic-container">
                <table>
                    <tbody>
                        <tr>
                            <th>Keyword</th>
                            <th>Total</th>
                            <th>Date</th>
                        </tr>
                        {data.map((res, index) =>
                            <tr key={index}>
                                <td>{res.keyword}</td>
                                <td>{res.total}</td>
                                <td>{res.searchDate}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}

StatisticContent.propTypes = {
    /**
     * Items.
     */
    data: PropTypes.array.isRequired,
    isError: PropTypes.string
}

/**
 * Default values
 */
StatisticContent.defaultProps = {
    loading: false,
    isError: null
}

export default StatisticContent;