import React from 'react';
import { shallow } from 'enzyme';
import StatisticContent from './';



it('renders without crashing', () => {
    shallow(<StatisticContent data={[]} />);
});

it('renders error content', () => {
    const wrapper = shallow(<StatisticContent data={[]} loading={false}  isError={"No Result"}/>);
    const noResult = <div>No Result</div>;
    expect(wrapper.contains(noResult)).toEqual(true);
});